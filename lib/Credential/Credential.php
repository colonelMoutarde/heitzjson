<?php

/*
 * Copyright (C) 2015 luc SANCHEZ.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

namespace JsonHeitz\Credential;

use JsonHeitz\JsonHeitzException\JsonHeitzException;

/**
 * Description of Credential
 *
 * @author luc
 */
class Credential
{

    /**
     * @abstract port à définir suivant les besoins de la configuration du serveur WEB HEITZ, par défault c'est le port web standard 80
     * @var int
     */
    private $port = 80;

    /**
     *
     * @var string $host
     */
    private $host;

    /**
     *
     * @var string Host Password
     */
    private $hostPassword;

    /**
     *
     * @var string User Login
     */
    private $userLogin;

    /**
     *
     * @var string User pass
     */
    private $userPass;

    /**
     *
     * @var string $urlWebServeurHeitz
     */
    private $urlWebServeurHeitz;

    /**
     * @name getPort()
     * @access public
     * @return object $this
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * @name getHost()
     * @access public
     * @return object $this
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @name getHostPassword()
     * @access public
     * @return object $this
     */
    public function getHostPassword()
    {
        return $this->hostPassword;
    }

    /**
     * @name getUserLogin()
     * @access public
     * @return object $this
     */
    public function getUserLogin()
    {
        return $this->userLogin;
    }

    /**
     * @name getUserPass()
     * @access public
     * @return object $this
     */
    public function getUserPass()
    {
        return $this->userPass;
    }

    /**
     * @name setPort()
     * @access public
     * @param int $port
     * @return int port
     * @throws JsonHeitzException
     */
    public function setPort($port)
    {
        if (((!empty($port)) && (filter_var($port, FILTER_VALIDATE_INT) ) && ($port > 0 && $port <= 65535))) {
            $this->port = $port;
            return $this;
        } else {
            throw new JsonHeitzException(__METHOD__ . ' --> The port is not a interger or is empty !', 1);
        }
    }

    /**
     * @name setHost()
     * @access public
     * @return string $host
     * @throws JsonHeitzException
     */
    public function setHost($host)
    {
        if (!empty($host)) {
            $this->host = $host;
            return $this;
        } else {
            throw new JsonHeitzException(__METHOD__ . ' --> Domain or IP are empty !', 2);
        }
    }

    /**
     * @name setHostPassword()
     * @access public
     * @return string $hostPassword
     * @throws JsonHeitzException
     */
    public function setHostPassword($hostPassword)
    {
        if (!empty($hostPassword)) {
            $this->hostPassword = $hostPassword;
            return $this;
        } else {
            throw new JsonHeitzException(__METHOD__ . ' --> Password is empty !', 3);
        }
    }

    /**
     * @name setUserLogin()
     * @access public
     * @return string $userLogin
     * @throws JsonHeitzException
     */
    public function setUserLogin($userLogin)
    {
        if (!empty($userLogin) && (filter_var($userLogin, FILTER_VALIDATE_EMAIL))) {
            $this->userLogin = $userLogin;
            return $this;
        } else {
            throw new JsonHeitzException(__METHOD__ . ' --> Email is empty or invalid !', 4);
        }
    }

    /**
     * @name setUserPass()
     * @access public
     * @return string $userPass
     * @throws JsonHeitzException
     */
    public function setUserPass($userPass)
    {
        if (!empty($userPass)) {
            $this->userPass = $userPass;
            return $this;
        } else {
            throw new JsonHeitzException(__METHOD__ . ' --> User pass is empty !', 5);
        }
    }

    /**
     * @name urlWebServeurHeitz()
     * @access public
     * @return string $urlWebServeurHeitz
     */
    public function urlWebServeurHeitz()
    {
        $this->urlWebServeurHeitz = (string) $this->getHost() . ':' . $this->getPort();
        return $this;
    }

    /**
     * @name __toString()
     * @access public
     * @return type
     */
    public function __toString()
    {
        return $this->urlWebServeurHeitz;
    }

}
