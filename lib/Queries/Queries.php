<?php

/*
 * Copyright (C) 2016 luc SANCHEZ.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

namespace JsonHeitzV2\Queries;

use JsonHeitzV2\JsonHeitzV2,
    JsonHeitz\JsonHeitzException\JsonHeitzException,
    JsonHeitz\Credential\Credential;

/**
 * Description of JsonHeitzV2Queries
 *
 * @author luc
 */
class Queries extends JsonHeitzV2
{

    private $credential;

    /**
     * @access public
     * @param Credential $credential
     * @return $this
     */
    public function __construct(Credential $credential)
    {
        parent::__construct($credential);
        $this->credential = $credential;
        return $this;
    }

    /**
     * @name getTaskList()
     * @access public
     * @param type idcentre(optional)
     * @return array query with id 20003
     */
    public function getTaskList($idCentre = NULL)
    {
        if ($idCentre !== null) {
            $param = ['idCentre' => $idCentre];
        } else {
            $param = null;
        }

        return $this->callWSHeitz((array) $this->query(20003, $param));
    }

    /**
     * @name  getActiveReservation()
     * @access public
     * @param int $idRequete
     * @param int $idCentre
     * @param int $nombreJourPlanning
     * @param \DateTime $datePlanning
     * @return array query with id 20004
     * 
     */
    public function getReservationListAvailable($idRequete = 0, $idCentre, $nombreJourPlanning = 7, \DateTime $datePlanning)
    {
        $array = [
            'idRequete'          => $idRequete,
            'idCentre'           => $idCentre,
            'nombreJourPlanning' => $nombreJourPlanning,
            'datePlanning'       => $datePlanning->format('d-m-y')
        ];

        return $this->callWSHeitz((array) $this->query(20004, $array));
    }

    /**
     * @name  getActiveReservation()
     * @access public
     * @return array query with id 20005
     */
    public function getActiveReservation()
    {
        return $this->callWSHeitz((array) $this->query(20005));
    }

    /**
     * @name  getFormTraining()
     * @access public
     * @return array query with id 20006
     */
    public function getFormTraining()
    {
        return $this->callWSHeitz((array) $this->query(20006));
    }

    /**
     * @name  getFormTraining()
     * @access public
     * @param int $idRequete
     * @return array query with id 20007
     */
    public function getDetailOfTheFormTraining($idRequete)
    {
        if (empty($idRequete)) {

            throw new JsonHeitzException(__METHOD__ . ' An element is empty', 9);
        } else {
            $array = [
                'idRequete' => $idRequete
            ];
            return $this->callWSHeitz((array) $this->query(20007, $array));
        }
    }

    /**
     * @name  getFormTraining()
     * @access public
     * @param int $idRequete
     * @return array query with id 20008
     * @throws JsonHeitzException
     */
    public function getDetailOfBilan($idRequete)
    {
        if (empty($idRequete)) {

            throw new JsonHeitzException(__METHOD__ . ' An element is empty', 9);
        } else {
            $array = [
                'idRequete' => $idRequete
            ];
            return $this->callWSHeitz((array) $this->query(20008, $array));
        }
    }

    /**
     * @name  getDetailOfTest()
     * @access public
     * @param int $idRequete
     * @return array query with id 20009
     * @throws JsonHeitzException
     */
    public function getDetailOfTest($idRequete)
    {
        if (empty($idRequete)) {

            throw new JsonHeitzException(__METHOD__ . ' --> An element is empty in the method !', 9);
        } else {
            $array = [
                'idRequete' => $idRequete
            ];
            return $this->callWSHeitz((array) $this->query(20009, $array));
        }
    }

    /**
     * @name  getListOfCures()
     * @access public
     * @return array query with id 20010
     */
    public function getListOfCures()
    {
        return $this->callWSHeitz((array) $this->query(20010));
    }

    /**
     * @name  getDetailOfCures()
     * @access public
     * @return array query with id 20011
     */
    public function getDetailOfCures()
    {
        return $this->callWSHeitz((array) $this->query(20011));
    }

    /**
     * @name  getDetailOfTest()
     * @access public
     * @param type $idRequete
     * @param \DateTime $dateStart
     * @param int $numberOfDay
     * @param array $groups
     * @param array $task
     * @param array $dates
     * @param array $periodes
     * @param array $dayOfWeek
     * @return array query with id 20012
     * @throws JsonHeitzException
     */
    public function getListOfActiveReservationsForATaskAndADate($idRequete, \DateTime $dateStart, $numberOfDay = 1, array $groups, array $task, array $dates, array $periodes, array $dayOfWeek)
    {

        $array = [
            'idRequete'    => $idRequete,
            'debut'        => $dateStart->format('d-m-Y'),
            'nombreJour'   => $numberOfDay,
            'groupes'      => $groups,
            'taches'       => $task,
            'dates'        => $dates,
            'periodes'     => $periodes,
            'joursSemaine' => $dayOfWeek,
        ];
        return $this->callWSHeitz((array) $this->query(20012, $array));
    }

    /**
     * @name  getConfigServeur()
     * @access public
     * @return array query with id 20013
     */
    public function getConfigServeur()
    {
        return $this->callWSHeitz((array) $this->query(20013));
    }

    /**
     * @name  getBilan()
     * @access public
     * @return array query with id 20014
     */
    public function getBilan()
    {
        return $this->callWSHeitz((array) $this->query(20014));
    }

    /**
     * @name  getTest()
     * @access public
     * @return array query with id 20015
     */
    public function getTest()
    {
        return $this->callWSHeitz((array) $this->query(20015));
    }

    /**
     * @name getOneClient()
     * @access public
     * @return array query with id 20016
     */
    public function getOneClient()
    {
        return $this->callWSHeitz((array) $this->query(20016));
    }

    /**
     * @name getClientMessage()
     * @access public
     * @param int $typeMessage 10
     * @param int $idClientMessage
     * @return array query with id 20017
     */
    public function getClientMessage($typeMessage = 10, $idClientMessage)
    {
        $fields = [
            'typeMessage'     => $typeMessage,
            'idClientMessage' => $idClientMessage,
        ];
        return $this->callWSHeitz((array) $this->query(20017, $fields));
    }

    /**
     * @name getListOfActiveAccess()
     * @access public
     * @return array query with id 20018
     */
    public function getListOfActiveAccess()
    {
        return $this->callWSHeitz((array) $this->query(20018));
    }

    /**
     * @name getListOfPassages()
     * @access public
     * @return array query with id 20019
     */
    public function getListOfPassages()
    {
        return $this->callWSHeitz((array) $this->query(20019));
    }

    /**
     * @name getListOfLevies()
     * @access public
     * @return array query with id 20020
     */
    public function getListOfLevies()
    {
        return $this->callWSHeitz((array) $this->query(20020));
    }

    /**
     * @name getListOfFinancialDeadlines()
     * @access public
     * @return array query with id 20021
     */
    public function getListOfFinancialDeadlines()
    {
        return $this->callWSHeitz((array) $this->query(20021));
    }

    /**
     * @name getListOfBills()
     * @access public
     * @return array query with id 20022
     */
    public function getListOfBills()
    {
        return $this->callWSHeitz((array) $this->query(20022));
    }

    /**
     * @name getListOfFinancial()
     * @access public
     * @return array query with id 20023
     */
    public function getListOfFinancial()
    {
        return $this->callWSHeitz((array) $this->query(20023));
    }

    /**
     * @name getListOfPoints()
     * @access public
     * @return array query with id 20024
     */
    public function getListOfPoints()
    {
        return $this->callWSHeitz((array) $this->query(20024));
    }

    /**
     * @name getScheduleSEPA()
     * @access public
     * @return array query with id 20025
     */
    public function getScheduleSEPA()
    {
        return $this->callWSHeitz((array) $this->query(20025));
    }

    /**
     * @name getScheduleUser()
     * @access public
     * @return array query with id 20030
     * @throws JsonHeitzException
     */
    public function getScheduleUser(\DateTime $smallDateStart, \DateTime $smallDateEnd)
    {
        if (!( $smallDateStart && $smallDateEnd )) {

            throw new JsonHeitzException(__METHOD__ . ' --> An element is empty in params !', 9);
        }

        $array = [
            'debut' => $smallDateStart->format('d-m-Y'),
            'fin'   => $smallDateEnd->format('d-m-Y'),
        ];
        return $this->callWSHeitz((array) $this->query(20030, $array));
    }

    /**
     * @name getDashboard()
     * @access public
     * @param \DateTime $dateStart
     * @param \DateTime $dateEnd
     * @return array query with id 20031
     * @throws JsonHeitzException
     */
    public function getDashboard(\DateTime $dateStart, \DateTime $dateEnd)
    {
        if (!( $dateStart && $dateEnd )) {

            throw new JsonHeitzException(__METHOD__ . ' --> An element is empty params !', 9);
        }
        $fields = [
            'debut' => $dateStart->format('d-m-Y'),
            'fin'   => $dateEnd->format('d-m-Y'),
        ];
        return $this->callWSHeitz((array) $this->query(20031, $fields));
    }

    /**
     * @name getDashbordFinancial()
     * @access public
     * @param \DateTime $date
     * @return array query with id 20032
     * @throws Exception Date is empty !
     */
    public function getDashbordFinancial(\DateTime $date)
    {
        $array = [];

        if (isset($date)) {
            $array['date'] = $date->format('d-m-Y');
        } else {
            throw new JsonHeitzException('Date is empty !', 9);
        }

        return $this->callWSHeitz((array) $this->query(20032, $array));
    }

    /**
     * @name getFinancial()
     * @access public
     * @param \DateTime $dateStart
     * @param \DateTime $dateEnd
     * @return array query with id 20033
     */
    public function getFinancial(\DateTime $dateStart, \DateTime $dateEnd)
    {
        if (!( $dateStart && $dateEnd )) {
            throw new JsonHeitzException(__METHOD__ . ' --> An element is empty in params !', 9);
        } else {
            $fields = [
                'debut' => $dateStart->format('d-m-Y'),
                'fin'   => $dateEnd->format('d-m-Y'),
            ];
            return $this->callWSHeitz((array) $this->query(20033, $fields));
        }
    }

    /**
     * @name getSummaryEmployee()
     * @access public
     * @param \DateTime $dateStart
     * @param \DateTime $dateEnd
     * @return array query with id 20034
     * @throws JsonHeitzException
     */
    public function getSummaryEmployee(\DateTime $dateStart, \DateTime $dateEnd)
    {
        if (!( $dateStart && $dateEnd )) {

            throw new JsonHeitzException(__METHOD__ . ' --> An element is empty in params !', 9);
        } else {
            $array = [
                'debut' => $dateStart->format('d-m-Y'),
                'fin'   => $dateEnd->format('d-m-Y')
            ];
            return $this->callWSHeitz((array) $this->query(20034, $array));
        }
    }

    /**
     * @name getReadingPointsBalance()
     * @access public
     * @param int $idRequete
     * @param int $pointIDClient
     * @return array query with id 20035
     * @throws JsonHeitzException
     */
    public function getReadingPointsBalance($idRequete, $pointIDClient)
    {
        if (!( $idRequete && $pointIDClient )) {

            throw new JsonHeitzException(__METHOD__ . ' --> An element is empty in the methode !', 9);
        } else {
            $fields = [
                'idRequete'     => $idRequete,
                'pointIDClient' => $pointIDClient
            ];
            return $this->callWSHeitz((array) $this->query(20035, $fields));
        }
    }

    /**
     * @name removingPointOfACustomer()
     * @access public
     * @param int $idRequete
     * @param int $pointIDClient
     * @param int $point
     * @return array query with id 20036
     * @throws JsonHeitzException
     */
    public function removingPointOfACustomer($idRequete, $pointIDClient, $point)
    {
        if (!( $idRequete && $pointIDClient && $point )) {

            throw new JsonHeitzException(__METHOD__ . ' --> An element is empty in params !', 9);
        } else {
            $array = [
                'idRequete'     => $idRequete,
                'pointIDClient' => $pointIDClient,
                'point'         => $point
            ];
            return $this->callWSHeitz((array) $this->query(20036, $array));
        }
    }

    /**
     * @name getAllCivility()
     * @access public
     * @return array query with id 20101
     */
    public function getAllCivility()
    {
        return $this->callWSHeitz((array) $this->query(20101));
    }

    /**
     * @name getAllJobs()
     * @access public
     * @return array query with id 20102
     */
    public function getAllJobs()
    {
        return $this->callWSHeitz((array) $this->query(20102));
    }

    /**
     * @name getAllCity()
     * @access public
     * @return array query with id 20103
     */
    public function getAllCity()
    {
        return $this->callWSHeitz((array) $this->query(20103));
    }

    /**
     * @name getFamilySituation()
     * @access public
     * @return array query with id 20104
     */
    public function getAllFamilySituation()
    {
        return $this->callWSHeitz((array) $this->query(20104));
    }

    /**
     * @name getEmployee()
     * @access public
     * @return array query with id 20105
     */
    public function getAllEmployee()
    {
        return $this->callWSHeitz((array) $this->query(20105));
    }

    /**
     * @name getCustomerGroup()
     * @access public
     * @return array query with id 20106
     */
    public function getAllCustomerGroup()
    {
        return $this->callWSHeitz((array) $this->query(20106));
    }

    /**
     * @name getDiscountLevel()
     * @access public
     * @return array query with id 20107
     */
    public function getAllDiscountLevel()
    {
        return $this->callWSHeitz((array) $this->query(20107));
    }

    /**
     * @name getTypeOfProspects()
     * @access public
     * @return array query with id 20108
     */
    public function getAllTypeOfProspects()
    {
        return $this->callWSHeitz((array) $this->query(20108));
    }

    /**
     * @name getWayToKnowTheInstitution()
     * @access public
     * @return array query with id 20109
     */
    public function getAllWayToKnowTheInstitution()
    {
        return $this->callWSHeitz((array) $this->query(20109));
    }

    /**
     * @name getWhereTheCustomerHasPracticed()
     * @access public
     * @return array query with id 20110
     */
    public function getAllWhereTheCustomerHasPracticed()
    {
        return $this->callWSHeitz((array) $this->query(20110));
    }

    /**
     * @name getMotivation()
     * @access public
     * @return array query with id 20111
     */
    public function getAllMotivation()
    {
        return $this->callWSHeitz((array) $this->query(20111));
    }

    /**
     * @name getCriterion1()
     * @access public
     * @return array query with id 20112
     */
    public function getCriterion1()
    {
        return $this->callWSHeitz((array) $this->query(20112));
    }

    /**
     * @name getCriterion2()
     * @access public
     * @return array query with id 20113
     */
    public function getCriterion2()
    {
        return $this->callWSHeitz((array) $this->query(20113));
    }

    /**
     * @name getCriterion3()
     * @access public
     * @return array query with id 20114
     */
    public function getCriterion3()
    {
        return $this->callWSHeitz((array) $this->query(20114));
    }

    /**
     * @name getCriterion4()
     * @access public
     * @return array query with id 20115
     */
    public function getCriterion4()
    {
        return $this->callWSHeitz((array) $this->query(20115));
    }

    /**
     * @name getCriterion5()
     * @access public
     * @return array query with id 20116
     */
    public function getCriterion5()
    {
        return $this->callWSHeitz((array) $this->query(20116));
    }

    /**
     * @name getArticles()
     * @access public
     * @return array query with id 20117
     */
    public function getAllArticles()
    {
        return $this->callWSHeitz((array) $this->query(20117));
    }

    /**
     * @name getVAT()
     * @access public
     * @return array query with id 20118
     */
    public function getAllVAT()
    {
        return $this->callWSHeitz((array) $this->query(20118));
    }

    /**
     * @name getPayment()
     * @access public
     * @return array query with id 20119
     */
    public function getAllPayment()
    {
        return $this->callWSHeitz((array) $this->query(20119));
    }

    /**
     * @name getArrowPoints()
     * @access public
     * @return array query with id 20120
     */
    public function getArrowPoints()
    {
        return $this->callWSHeitz((array) $this->query(20120));
    }

    /**
     * @name getTaskGroup()
     * @access public
     * @return array query with id 20121
     */
    public function getTaskGroup()
    {
        return $this->callWSHeitz((array) $this->query(20121));
    }

    /**
     * @name getPlaceForTheTask()
     * @access public
     * @return array query with id 20122
     */
    public function getPlaceForTheTask()
    {
        return $this->callWSHeitz((array) $this->query(20122));
    }

    /**
     * @name setPassword()
     * @access public
     * @param string $email
     * @param string $oldPassword
     * @param string $newPassword
     * @param string $confirmPassword
     * @return array the query with id 20201
     * @throws JsonHeitzException 'An element is empty in methode setPassword() !
     */
    public function setPassword($email, $oldPassword, $newPassword, $confirmPassword)
    {
        if (!( $email && $oldPassword && $newPassword && $confirmPassword )) {

            throw new JsonHeitzException(__METHOD__ . ' --> An element is empty in the method !', 9);
        } else {
            $this->lenghtOfString($newPassword, 20);
            $fields = [
                'email'        => $email,
                'code'         => $oldPassword,
                'nouveau'      => $newPassword,
                'confirmation' => $confirmPassword
            ];

            return $this->callWSHeitz((array) $this->query(20201, $fields));
        }
    }

    /**
     * @name getMessage()
     * @param int $id
     * @return array the query with id 20202
     * @throws JsonHeitzException
     */
    public function getMessage($id)
    {

        if (!($id)) {

            throw new JsonHeitzException(__METHOD__ . ' --> Param is empty !', 9);
        } else {
            $field = [
                'idRequete' => $id,
            ];
            return $this->callWSHeitz((array) $this->query(20202, $field));
        }
    }

    /**
     * @name setClient()
     * @access public
     * @param array $array
     * @return array query with id 20203
     * @throws JsonHeitzException 'Bad array !'
     */
    public function setClient(array $array)
    {
        if (count($array) <= 21) {
            return $this->callWSHeitz((array) $this->query(20203, $array));
        } else {
            throw new JsonHeitzException(__METHOD__ . ' -->  Bad array !', 10);
        }
    }

    /**
     * @name setReservation()
     * @param int $id
     * @param bool $state
     * @param string $describ
     * @return array query with id 20204
     * @throws JsonHeitzException
     */
    public function setReservation($id, $state, $describ)
    {
        if (!($id && $state && $describ)) {
            $fields = [
                'idRequete'        => $id,
                'annulation'       => $state,
                'raisonAnnulation' => $describ,
            ];
            return $this->callWSHeitz((array) $this->query(20204, $fields));
        } else {
            throw new JsonHeitzException(__METHOD__ . ' -->  Bad array !', 10);
        }
    }

    /**
     * @name addReservation()
     * @access public
     * @param array $array
     * @return array query with id 20301
     * @throws JsonHeitzException 'Bad array !'
     */
    public function addReservation(array $array)
    {
        if (count($array) == 6) {
            return $this->callWSHeitz((array) $this->query(20301, $array));
        } else {
            throw new JsonHeitzException(__METHOD__ . ' --> Bad array !', 10);
        }
    }

    /**
     * @name addVente()
     * @access public
     * @param array $array
     * @return array query with id 20302
     * @throws JsonHeitzException 'Bad array !'
     */
    public function addSale(array $array)
    {
        if (count($array) == 2) {
            return $this->callWSHeitz((array) $this->query(20302, $array));
        } else {
            throw new JsonHeitzException(__METHOD__ . ' --> Bad array', 10);
        }
    }

    /**
     * @name addClient()
     * @access public
     * @param array $array
     * @return array query with id 20303
     * @throws JsonHeitzException 'Bad array !'
     */
    public function addClient(array $array)
    {
        if (count($array) >= 4) {
            return $this->callWSHeitz((array) $this->query(20303, $array));
        } else {
            throw new JsonHeitzException(__METHOD__ . ' -->  Bad array !', 10);
        }
    }

    /**
     * @name addPassword()
     * @access public
     * @param string email
     * @return array query with id 20401
     * @throws JsonHeitzException 'Email is empty !'
     */
    public function addPassword($email)
    {
        $field = [];
        if (!empty($email) && (filter_var($email, FILTER_VALIDATE_EMAIL))) {
            $field['email'] = $email;
        } else {
            throw new JsonHeitzException(__METHOD__ . ' --> Email is empty or invalid !', 4);
        }

        return $this->callWSHeitz((array) $this->query(20401, $field));
    }

    /**
     * @name searchClient()
     * @access public
     * @param array $array
     * @return array query with id 20501
     * @throws JsonHeitzException 'Item "operateurChaine" in empty in searchClient() !'
     */
    public function searchClient(array $array, $operateurChaine, $nombreEnregistrement = null)
    {
        if ((!array_key_exists('operateurChaine', $array) ) || ($operateurChaine !== null)) {

            if (($nombreEnregistrement !== null) && is_integer($nombreEnregistrement)) {
                $array['nombreEnregistrement'] = $nombreEnregistrement;
            }

            $array['operateurChaine'] = $operateurChaine;
        } else {
            throw new JsonHeitzException(__METHOD__ . ' --> Item "operateurChaine" is empty !', 22);
        }
        return $this->callWSHeitz((array) $this->query(20501, $array));
    }

    /**
     * @name SQLQuery()
     * @access public
     * @param string $sql
     * @return array query with id 20502
     * @throws JsonHeitzException 'Query SQL is empty !'
     */
    public function SQLQuery($sql)
    {
        $array = [];
        if (!empty($sql)) {

            $array['requeteSQL'] = $sql;
        } else {
            throw new JsonHeitzException(__METHOD__ . ' --> Query SQL is empty !', 30);
        }

        return $this->callWSHeitz((array) $this->query(20502, $array));
    }

    /**
     * @name numberOfClientsInEstablishing()
     * @access public
     * @param int $idCenter
     * @return array
     */
    public function numberOfClientsInEstablishing($idCenter)
    {

        return $this->SQLQuery('SELECT Nombre From WS_Client_Present_Nombre(' . $idCenter . ')');
    }

    /**
     * @name getSalesOver()
     * @access public
     * @param \DateTime $dateStart
     * @param \DateTime $dateEnd
     * @param int $idCenter
     * @return array
     */
    public function getSalesOver(\DateTime $dateStart, \DateTime $dateEnd, $idCenter = 0)
    {

        return $this->SQLQuery('SELECT Nombre, Montant_ttc From WS_Vente_Periode(' . $idCenter . ", '" . $dateStart->format('Y-m-d') . "' , '" . $dateEnd->format('Y-m-d') . "')");
    }

    /**
     * @name regulationsOver()
     * @access public
     * @param \DateTime $dateStart
     * @param \DateTime $dateEnd
     * @param int $idCenter
     * @return array
     */
    public function regulationsOver(\DateTime $dateStart, \DateTime $dateEnd, $idCenter = 0)
    {
        return $this->SQLQuery('SELECT Nombre, Montant From WS_Reglement_Periode(' . $idCenter . ", '" . $dateStart->format('Y-m-d') . "' , '" . $dateEnd->format('Y-m-d') . "')");
    }

    /**
     * @name getClientIdByEmail()
     * @access public
     * @param string $email
     * @return type
     * @throws JsonHeitzException
     */
    public function getClientIdByEmail($email)
    {

        if (!empty($email) && (filter_var($email, FILTER_VALIDATE_EMAIL))) {
            return $this->SQLQuery("Select IDClient From WS_IDClient_Email('$email')");
        } else {
            throw new JsonHeitzException(__METHOD__ . ' --> Email is empty or invalid !', 4);
        }
    }

    /**
     * @name clientWithAnActiveSpecificAccess()
     * @access public
     * @param int $idClient
     * @param int $idAcces
     * @return array
     */
    public function clientWithAnActiveSpecificAccess($idClient, $idAcces)
    {
        return $this->SQLQuery("Select Actif From WS_Client_Acces_Actif('" . (int) $idClient . "', '" . (int) $idAcces . "')");
    }

    /**
     * @name accessListForAClient()
     * @access public
     * @param integer $idClient
     * @return array
     */
    public function accessListForAClient($idClient)
    {
        return $this->SQLQuery("Select 
                                Acces_ID, 
                                Article_ID, 
                                Acces_Nom, 
                                Acces_Debut, 
                                Acces_Fin, 
                                Acces_Renouvellement,
                                Acces_Suspension, 
                                Acces_Contrat, 
                                Acces_Archive, 
                                Acces_Resiliation 
                                From
                                WS_Client_Liste_Acces(" . (int) $idClient . ")");
    }

    /**
     * @name listOfProspectsCreatedOver()
     * @access public
     * @param \DateTime $dateStart
     * @param \DateTime $dateEnd
     * @param bool $photo optional, default false
     * @return array
     */
    public function listOfProspectsCreatedOver(\DateTime $dateStart, \DateTime $dateEnd, $photo = false)
    {

        return $this->SQLQuery("Select 
                                    Client_ID, 
                                    " . $this->getPhoto($photo) . " 
                                    Client_Nom, 
                                    Client_Prenom, 
                                    Civilite_ID, 
                                    Civilite_Libelle,
                                    Client_Date_Naissance, 
                                    Client_Date_Creation,
                                    Client_Telephone, 
                                    Client_Portable, 
                                    Client_Adresse1,
                                    Client_Adresse2, 
                                    Client_Adresse3, 
                                    Client_Adresse4, 
                                    Ville_ID, 
                                    Ville_Nom, 
                                    Client_Email,
                                    Client_Contact_SMS, 
                                    Client_Contact_Email, 
                                    Client_Contact_Courrier, 
                                    Client_Contact_Telephone,
                                    Client_Code_Reservation 
                                    From 
                                    WS_Prospect_Creation('" . $dateStart->format('Y-m-d') . "', '" . $dateEnd->format('Y-m-d') . "')");
    }

    /**
     * @name listOfClientsCreatedOver()
     * @access public
     * @param \DateTime $dateStart
     * @param \DateTime $dateEnd
     * @param bool $photo optional, default false
     * @return array
     */
    public function listOfClientsCreatedOver(\DateTime $dateStart, \DateTime $dateEnd, $photo = false)
    {

        return $this->SQLQuery('select
                                        Client_ID,
                                        ' . $this->getPhoto($photo) . " 
                                        Client_Nom,
                                        Client_Prenom,
                                        Civilite_ID,
                                        Civilite_Libelle,
                                        Client_Date_Naissance,
                                        Client_Date_Creation,
                                        Client_Telephone,
                                        Client_Portable,
                                        Client_Adresse1,
                                        Client_Adresse2,
                                        Client_Adresse3,
                                        Client_Adresse4,
                                        Ville_ID,
                                        Ville_Nom,
                                        Client_Email,
                                        Client_Contact_SMS,
                                        Client_Contact_Email,
                                        Client_Contact_Courrier,
                                        Client_Contact_Telephone,
                                        Client_Code_Reservation
                                        From 
                                        WS_Client_Creation( '" . $dateStart->format('Y-m-d') . "', '" . $dateEnd->format('Y-m-d') . "')");
    }

    /**
     * @name listOfProspectsConvertedOver()
     * @access public
     * @param \DateTime $dateStart
     * @param \DateTime $dateEnd
     * @param bool $photo
     * @return array
     */
    public function listOfProspectsConvertedOver(\DateTime $dateStart, \DateTime $dateEnd, $photo = false)
    {

        return $this->SQLQuery('Select 
                                        Client_ID,
                                        ' . $this->getPhoto($photo) . "
                                        Client_Nom,
                                        Client_Prenom,
                                        Civilite_ID,
                                        Civilite_Libelle,
                                        Client_Date_Naissance,
                                        Client_Date_Creation,
                                        Client_Telephone,
                                        Client_Portable,
                                        Client_Adresse1,
                                        Client_Adresse2,
                                        Client_Adresse3,
                                        Client_Adresse4,
                                        Ville_ID,
                                        Ville_Nom,
                                        Client_Email,
                                        Client_Contact_SMS,
                                        Client_Contact_Email,
                                        Client_Contact_Courrier,
                                        Client_Contact_Telephone,
                                        Client_Code_Reservation
                                        From
                                        WS_Client_Conversion('" . $dateStart->format('Y-m-d') . "', '" . $dateEnd->format('Y-m-d') . "')");
    }

    /**
     * @name listOfActiveSites()
     * @access public
     * @param array $task
     * @param array $dates
     * @param array $periods
     * @return type
     */
    public function listOfActiveSites(array $task, array $dates, array $periods)
    {
        $fields = [
            'taches'   => $task,
            'dates'    => $dates,
            'periodes' => $periods,
        ];
        return $this->callWSHeitz((array) $this->query(21001, $fields));
    }

    /**
     * @name setDataClients()
     * @access public
     * @param string $key
     * @param string $value
     * @return array query with id 21002
     * @throws JsonHeitzException an element is empty in params
     */
    public function setDataClients($key, $value)
    {
        if (!( $key && $value )) {
            throw new JsonHeitzException(__METHOD__ . ' --> An element is empty in params !', 9);
        } else {
            $fields = [
                'cle'    => $key,
                'valeur' => $value,
            ];
            return $this->callWSHeitz((array) $this->query(21002, $fields));
        }
    }

    /**
     * @name getDataClients() 
     * @access public
     * @param string $key
     * @return array query with id 21003
     * @throws JsonHeitzException An element is empty in param !
     */
    public function getDataClients($key)
    {
        if (!($key)) {
            throw new JsonHeitzException(__METHOD__ . ' --> An element is empty in param !', 9);
        } else {
            $field = [
                'cle' => $key,
            ];
            return $this->callWSHeitz((array) $this->query(21003, $field));
        }
    }

    /**
     * @name setNewPasswordForClientId()
     * @access public
     * @param id $idClient
     * @param string $newPassword
     * @return array query with id 21005
     * @throws JsonHeitzException An element is empty in params !
     */
    public function setNewPasswordForClientId($idClient, $newPassword)
    {
        if (!($idClient && $newPassword)) {
            throw new JsonHeitzException(__METHOD__ . ' --> An element is empty in params !', 9);
        } else {
            $fields = [
                'idClient' => $idClient,
                'nouveau'  => $newPassword,
            ];
            return $this->callWSHeitz((array) $this->query(21005, $fields));
        }
    }

    /**
     * @name addPointsForClient()
     * @access public
     * @abstract the current user must be an employee
     * @param int $idQuery
     * @param int $idClient
     * @param int $point
     * @param string $describ
     * @return array query with id 21004
     */
    public function addPointsForClient($idQuery, $idClient, $point, $describ)
    {
        if (!( $idQuery && $idClient && $point && $describ )) {

            throw new JsonHeitzException(__METHOD__ . ' --> An element is empty in params !', 9);
        } else {
            $fields = [
                'idRequete'     => $idQuery,
                'pointIDClient' => $idClient,
                'point'         => $point,
                'libelle'       => $describ,
            ];
            return $this->callWSHeitz((array) $this->query(21004, $fields));
        }
    }

    /**
     * @name payboxForm()
     * @access public
     * @param array $sales 
     * @param string $return URL of return
     * @param string $cssClassButton
     * @return array query with id 21006
     * @throws JsonHeitzException
     */
    public function payboxForm(array $sales, $return, $cssClassButton = null)
    {
        if (!( $sales && $return )) {

            throw new JsonHeitzException(__METHOD__ . ' --> An element is empty in params !', 9);
        } else {
            $fields = [
                'ventes'       => $sales,
                'retour'       => $return,
                'classeBouton' => $cssClassButton,
            ];
            return $this->callWSHeitz((array) $this->query(21006, $fields));
        }
    }

    /**
     * @name payboxValidation()
     * @access public
     * @param string $refTransaction
     * @param string $sellerId
     * @param string $deal
     * @param string $errorCode
     * @param string $HMACkey
     * @return array query with id 21007
     * @throws JsonHeitzException An element is empty in params !
     */
    public function payboxValidation($refTransaction, $sellerId, $deal, $errorCode, $HMACkey)
    {
        if (!( $refTransaction && $sellerId && $deal && $errorCode && $HMACkey)) {

            throw new JsonHeitzException(__METHOD__ . ' --> An element is empty in params !', 9);
        } else {
            $fields = [
                'reference'      => $refTransaction,
                'identification' => $sellerId,
                'transaction'    => $deal,
                'erreur'         => $errorCode,
                'hmac'           => $HMACkey,
            ];
            return $this->callWSHeitz((array) $this->query(21007, $fields));
        }
    }

    /**
     * @name addBankAccount()
     * @access public
     * @param string $bankName
     * @param string $iban
     * @param string $bic
     * @param int $id
     * @return array query with id 21008
     * @throws JsonHeitzException
     */
    public function addBankAccount($bankName, $iban, $bic, $id)
    {
        if (!( $bankName && $iban && $bic && $id)) {

            throw new JsonHeitzException(__METHOD__ . ' --> An element is empty in params !', 9);
        } else {
            $fields = [
                'nomBanque' => $bankName,
                'iban'      => $iban,
                'bic'       => $bic,
                'idBanque'  => $id,
            ];
            return $this->callWSHeitz((array) $this->query(21008, $fields));
        }
    }

    /**
     * @name validateSession()
     * @access public
     * @return array query with id 21009
     */
    public function validateSession()
    {
        return $this->callWSHeitz((array) $this->query(21009));
    }

    /**
     * @name getConfigApp()
     * @access public
     * @return array query with id 21010
     */
    public function getConfigApp()
    {
        return $this->callWSHeitz((array) $this->query(21010));
    }

    /**
     * @name addSchedule
     * @param int $idTask
     * @param int $idLocation
     * @param int $idEmploye
     * @param \DateTime $dateStart
     * @param \DateTime $dateEnd
     * @param string $note
     * @param int $slots
     * @param int $idSerie
     * @param int $idEtablissement
     * @return array query with id 21011
     * @throws JsonHeitzException
     */
    public function addSchedule($idTask, $idLocation, $idEmploye = null, \DateTime $dateStart, \DateTime $dateEnd, $note = null, $slots, $idSerie = null, $idEtablissement = null)
    {
        if (!($idTask && $idLocation && $dateStart && $dateEnd && $slots)) {
            throw new JsonHeitzException(__METHOD__ . ' --> An element is empty in params !', 9);
        } else {
            $fields = [
                'idTache'         => (int) $idTask,
                'idLieu'          => (int) $idLocation,
                'idEmploye'       => $idEmploye,
                'debut'           => $dateStart->format('d-m-Y H:i:s'),
                'fin'             => $dateEnd->format('d-m-Y H:i:s'),
                'note'            => $note,
                'place'           => (int) $slots,
                'idSerie'         => (int) $idSerie,
                'idEtablissement' => (int) $idEtablissement,
            ];

            return $this->callWSHeitz((array) $this->query(21011, $fields));
        }
    }

    /**
     * @name setSchedule()
     * @param int $idSlot
     * @param int $idTask
     * @param int $idLocation
     * @param int $idEmploye
     * @param \DateTime $dateStart
     * @param \DateTime $dateEnd
     * @param string $note
     * @param int $slots
     * @param int $idSerie
     * @param int $idEtablissement
     * @return array query with id 21012
     * @throws JsonHeitzException
     */
    public function setSchedule($idSlot, $idTask, $idLocation, $idEmploye = null, \DateTime $dateStart, \DateTime $dateEnd, $note = null, $slots, $idSerie = null, $idEtablissement = null)
    {
        if (!($idSlot && $idTask && $idLocation && $dateStart && $dateEnd && $slots)) {
            throw new JsonHeitzException(__METHOD__ . ' --> An element is empty in params !', 9);
        } else {
            $fields = [
                'idCreneau'       => (int) $idSlot,
                'idTache'         => (int) $idTask,
                'idLieu'          => (int) $idLocation,
                'idEmploye'       => $idEmploye,
                'debut'           => $dateStart->format('d-m-Y H:i:s'),
                'fin'             => $dateEnd->format('d-m-Y H:i:s'),
                'note'            => $note,
                'place'           => (int) $slots,
                'idSerie'         => (int) $idSerie,
                'idEtablissement' => (int) $idEtablissement,
            ];

            return $this->callWSHeitz((array) $this->query(21012, $fields));
        }
    }

    /**
     * @name removeSchedule()
     * @param int $idSlot
     * @return array query with id 21013
     * @throws JsonHeitzException
     */
    public function removeSchedule($idSlot)
    {
        if (!($idSlot)) {
            throw new JsonHeitzException(__METHOD__ . ' --> Id slot is empty !', 9);
        } else {
            $field = [
                'idCreneau' => (int) $idSlot,
            ];

            return $this->callWSHeitz((array) $this->query(21013, $field));
        }
    }

    /**
     * @name listSchedule()
     * @param int $idSlot
     * @param \DateTime $dateStart
     * @param \DateTime $dateEnd
     * @return array query with id 21014
     * @throws JsonHeitzException
     */
    public function listSchedule($idSlot, \DateTime $dateStart, \DateTime $dateEnd)
    {
        if (!($idSlot && $dateStart && $dateEnd)) {
            throw new JsonHeitzException(__METHOD__ . ' --> Id slot is empty !', 9);
        } else {
            $fields = [
                'idCreneau' => (int) $idSlot,
                'debut'     => $dateStart->format('d-m-Y H:i:s'),
                'fin'       => $dateEnd->format('d-m-Y H:i:s'),
            ];

            return $this->callWSHeitz((array) $this->query(21014, $fields));
        }
    }

    /**
     * @name cancelSchedule()
     * @param int $idSlot
     * @param int $idEmploye
     * @param \DateTime $dateStart
     * @param \DateTime $dateEnd
     * @return array query with id 21015
     * @throws JsonHeitzException
     */
    public function cancelSchedule($idSlot, $idEmploye, \DateTime $dateStart, \DateTime $dateEnd)
    {
        if (!($idSlot && $idEmploye && $dateStart && $dateEnd)) {
            throw new JsonHeitzException(__METHOD__ . ' --> Id slot is empty !', 9);
        } else {
            $fields = [
                'idCreneau' => (int) $idSlot,
                'idEmploye' => (int) $idEmploye,
                'debut'     => $dateStart->format('d-m-Y H:i:s'),
                'fin'       => $dateEnd->format('d-m-Y H:i:s'),
            ];

            return $this->callWSHeitz((array) $this->query(21015, $fields));
        }
    }

    /**
     * @name getAllBank()
     * @access public
     * @return array query with id 21016
     */
    public function getAllBank()
    {
        return $this->callWSHeitz((array) $this->query(21016));
    }
    
    /**
     * @name addBank()
     * @access public
     * @param string $society
     * @param sting $code
     * @param string $bic
     * @param string $countryCode
     * @return array query with id 21017
     * @throws JsonHeitzException
     */
    public function addBank($society, $code, $bic, $countryCode)
    {
        if (!($society && $code && $bic && $countryCode)) {
            throw new JsonHeitzException(__METHOD__ . ' --> on param is empty !', 9);
        } else {
            $fields = [
                'societe'  => $society,
                'code'     => $code,
                'bic'      => $bic,
                'CodePays' => $countryCode,
            ];

            return $this->callWSHeitz((array) $this->query(21017, $fields));
        }
       
    }
    
    /**
     * 
     * @name addCity()
     * @access public
     * @param string $city
     * @param string $zipCode
     * @param string $country
     * @return array query with id 21018
     * @throws JsonHeitzException
     */
    public function addCity($city, $zipCode, $country)
    {
        if (!($city && $zipCode && $country)) {
            throw new JsonHeitzException(__METHOD__ . ' --> on param is empty !', 9);
        } else {
            $fields = [
                'ville'      => $city,
                'codePostal' => $zipCode,
                'pays'       => $country,
            ];

            return $this->callWSHeitz((array) $this->query(21018, $fields));
        }
    }
    /**
     * @name existingEmail()
     * @access public
     * @param string $email
     * @return array query with id 21019
     * @throws JsonHeitzException
     */
    public function existingEmail($email)
    {
        if (!empty($email) && (filter_var($email, FILTER_VALIDATE_EMAIL))) {
            throw new JsonHeitzException(__METHOD__ . ' --> on param is invalid !', 9);
        } else {
            $fields = [
                'email' => $email,
            ];

            return $this->callWSHeitz((array) $this->query(21019, $fields));
        }
    }
    
    /**
     * @name clientPresent()
     * @access public
     * @return type
     */
    public function clientPresent()
    {
        return $this->callWSHeitz((array) $this->query(21020));
    }
    
    /**
     * @name getAllTokens()
     * @access public
     * @return type
     */
    public function getAllTokens()
    {
        return $this->callWSHeitz((array) $this->query(21021));
    }
    
    /**
     * @name tokenActivation()
     * @access public
     * @param int $id
     * @param string $phoneCode
     * @return array query with id 21022
     * @throws JsonHeitzException
     */
    public function tokenActivation($id, $phoneCode)
    {
        if (!($id && $phoneCode)) {
            throw new JsonHeitzException(__METHOD__ . ' --> on param is invalid !', 9);
        } else {
            $fields = [
                'id'        => $id,
                'phoneCode' => $phoneCode,
            ];

            return $this->callWSHeitz((array) $this->query(21022, $fields));
        }
    }
    
    /**
     * @name tokenRevocation()
     * @access public
     * @param int $id
     * @param string $phoneCode
     * @return array query with id 21023
     * @throws JsonHeitzException
     */
    public function tokenRevocation($id, $phoneCode)
    {
        if (!($id && $phoneCode)) {
            throw new JsonHeitzException(__METHOD__ . ' --> on param is invalid !', 9);
        } else {
            $fields = [
                'id'        => $id,
                'phoneCode' => $phoneCode,
            ];

            return $this->callWSHeitz((array) $this->query(21023, $fields));
        }
    }
    
    /**
     * @name getCodePassing()
     * @access public
     * @return array query with id 21025
     */
    public function getCodePassing()
    {
        return $this->callWSHeitz((array) $this->query(21025));
    }
    
    /**
     * 
     * @param int $idType
     * @param array $fields
     * @return type
     */
    public function genericQuery($idType, array $fields)
    {
        return $this->callWSHeitz((array) $this->query($idType, $fields));
    }

    /**
     * @name disconnect()
     * @access public
     * @param string Path to Cookie
     * @return array query with id 20402
     */
    public function disconnect($path = 'tmp/cookie.txt')
    {
        $fp = fopen(realpath($path), 'r+');
        ftruncate($fp, 0);
        fclose($fp);
        return $this->callWSHeitz((array) $this->query(20402));
    }

}
