<?php

/*
 * Copyright (C) 2016 luc SANCHEZ.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *
 * MA 02110-1301  USA
 *
 * @author luc SANCHEZ <sanchezlucarobasegmaildotcom>
 * @copyright (c) 2016
 * @license GNU/LGPL http://www.gnu.org/licenses/lgpl.html 
 */

namespace JsonHeitzV2;

use JsonHeitz\ErrorDictionary\ErrorDictionary,
    JsonHeitz\JsonHeitzException\JsonHeitzException,
    JsonHeitz\Credential\Credential,
    JsonHeitzV2\Core\CoreInterface;

class JsonHeitzV2 implements CoreInterface
{

    /**
     *
     * @var string
     */
    private $connectHeitzAPI;

    /**
     *
     * @var int
     */
    private $idSession;

    /**
     *
     * @var int
     */
    private $idClient;

    /**
     *
     * @var bool
     */
    private $debug = false;

    /**
     *
     * @var string
     */
    private $credential;

    /**
     *
     * @var int 
     */
    private $numberRow;

    /**
     *
     * @var string
     */
    private $phoneCode;

    /**
     * 
     * @param Credential $credential
     * @return \JsonHeitzV2\JsonHeitzV2
     */
    public function __construct(Credential $credential)
    {
        $this->credential = $credential;
        return $this;
    }

    /**
     * @name getCredential();
     * @return object
     */
    public function getCredential()
    {
        return $this->credential;
    }

    /**
     * @name getIdSession()
     * @access public
     * @return object $this
     */
    public function getIdSession()
    {
        return $this->idSession;
    }

    /**
     * @name getIdClient()
     * @access public
     * @return object $this
     */
    public function getIdClient()
    {
        return $this->idClient;
    }

    /**
     * @name getDebug()
     * @access public
     * @return object $this
     */
    public function getDebug(): bool
    {
        return $this->debug;
    }

    /**
     * @name getConnectHeitzAPI()
     * @return object $this
     */
    public function getConnectHeitzAPI()
    {
        return $this->connectHeitzAPI;
    }

    /**
     * @name getNumberRow()
     * @return object $this
     */
    public function getNumberRow(): int
    {
        return $this->numberRow;
    }

    /**
     * 
     * @return type
     */
    public function getPhoneCode(): string
    {
        return $this->phoneCode;
    }

    /**
     * @name setIdSession()
     * @param string $idSession
     * @return \JsonHeitz\JsonHeitz
     */
    public function setIdSession($idSession)
    {
        $this->idSession = $idSession;
        return $this;
    }

    /**
     * @name setIdClient()
     * @access public
     * @param int $idClient idClient
     * @return $this
     * @throws JsonHeitzException
     */
    public function setIdClient($idClient)
    {
        if ((!empty($idClient)) && (filter_var($idClient, FILTER_VALIDATE_INT) )) {
            $this->idClient = $idClient;
            return $this;
        } else {
            throw new JsonHeitzException(__METHOD__ . ' --> The Client ID is empty or is not an integer !', 6);
        }
    }

    /**
     * @name setDebug()
     * @param bool $debug
     * @return \JsonHeitz\JsonHeitz
     */
    public function setDebug(bool $debug)
    {
        $this->debug = $debug;
        return $this;
        
    }

    /**
     * @name setNumberRow()
     * @access public
     * @param type int $numberRow
     * @return \JsonHeitz\JsonHeitz
     */
    public function setNumberRow(int $numberRow)
    {
         $this->numberRow = $numberRow;
         return $this;
        
    }

    /**
     * @name getPhoto()
     * @access protected
     * @param type $bool
     * @return string or Null
     */
    protected function getPhoto(bool $bool)
    {
        if ($bool === true) {
            $photo = 'Client_Photo,';
        } else {
            $photo = null;
        }
        return $photo;
    }

    /**
     * @name analyseReply()
     * @access private
     * @param string $JSON
     * @return \JsonHeitz\stdClass
     * @throws JsonHeitzException
     */
    private function analyseReply($JSON)
    {
        if (!is_object($JSON)) {
            $JSON           = new \stdClass();
            $JSON->status   = 'ko';
            $JSON->idErreur = '10';
        }

        if (!in_array('status', get_object_vars($JSON))) {

            $JSON           = new \stdClass();
            $JSON->status   = 'ko';
            $JSON->idErreur = '10';
        }

        if ($this->getDebug() === true) {
            var_dump($JSON);
        }

        if ($JSON->status === 'ok') {
            return $JSON;
        } elseif ($JSON->status === 'ko') {
            $error = (new ErrorDictionary($JSON->idErreur))->getMessage();
            throw new JsonHeitzException(__METHOD__ . ' --> ' . $error, $JSON->idErreur);
        } elseif ($JSON === null) {
            throw new JsonHeitzException(__METHOD__ . ' --> webservice returns an incorrect format !', 7);
        }
    }

    /**
     * @name setConnectHeitzAPI()
     * @access public
     * @param int $idSession
     * @param int $userId
     * @param int $type
     * @return $this array fields
     */
    public function setConnectHeitzAPI($idSession = 0, $userId = 0, $type = 1)
    {
        $fields                = ['status'    => 0,
            'idErreur'  => 0,
            'idSite'    => $this->credential->getHostPassword(),
            'type'      => $type,
            'email'     => $this->credential->getUserLogin(),
            'code'      => $this->credential->getUserPass(),
            'idSession' => $idSession,
            'idClient'  => $userId,
            'phoneCode' => $this->getPhoneCode(),
        ];
        $this->connectHeitzAPI = (array) $fields;
        return $this;
    }

    /**
     * 
     * @param string $phoneCode
     * @return $this
     * @throws JsonHeitzException
     */
    public function setPhoneCode(string $phoneCode)
    {

        $this->phoneCode = $phoneCode;
        return $this;
    }

    /**
     * @name callWSHeitz()
     * @access public
     * @param array fields
     * @param string pathToCookie
     * @return object json
     */
    public function callWSHeitz(string $newFields = null, string $pathToCookie = 'tmp/cookie.txt')
    {
        try {
            $curl = curl_init($this->credential->urlWebServeurHeitz() . '/json');

            if ($newFields === null) {
                $newFields = $this->getConnectHeitzAPI();
            }

            if ($this->getDebug() === true) {
                var_dump($newFields, json_encode($newFields));
            }

            //initialistion des options de cURL
            $options = [
                CURLOPT_POSTFIELDS     => json_encode($newFields),
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POST           => true,
                CURLOPT_HTTPHEADER     => ['Content-type: application/json'],
                CURLOPT_TIMEOUT        => 0,
                CURLOPT_FOLLOWLOCATION => 1,
                CURLOPT_COOKIEJAR      => realpath($pathToCookie),
                CURLOPT_COOKIEFILE     => realpath($pathToCookie)
            ];

            // Bind des options et de l'objet cURL que l'on va utiliser
            curl_setopt_array($curl, $options);

            $return = curl_exec($curl); // r�ponse de la requ�te dans la variable $return    
            //appel du gestionnaire des erreur de cURL
        } catch (JsonHeitzException $exc) {
            throw new JsonHeitzException(__METHOD__ . ' --> ' . $exc->getMessage(), $exc->getCode());
        }

        if (curl_errno($curl) > 0) {
            if ($this->getDebug() === true) {
                var_dump($curl);
            }
            $message = curl_error($curl);
            curl_close($curl);
            throw new JsonHeitzException(__METHOD__ . ' --> Value of cURL error: ' . $message, 8);
        } else {
            if ($this->getDebug() === true) {
                var_dump($curl);
            }
            curl_close($curl);
            return $this->analyseReply(json_decode($return));
        }
    }

    /**
     * @name query()
     * @access protected
     * @param int $type
     * @param int $param 
     * @return string 
     * @throws JsonHeitzException
     */
    protected function query($type, $param = null)
    {
        if ( empty($type) !== false) {

            $fields = [
                'status'    => 'null',
                'idErreur'  => 0,
                'idSite'    => $this->credential->getHostPassword(),
                'idSession' => $this->getIdSession(),
                'idClient'  => $this->getIdClient(),
                'type'      => $type,
                'phoneCode' => $this->getPhoneCode(),
            ];

            if ($this->getNumberRow() > 0) {
                $fields['nombreLigne'] = $this->getNumberRow();
            }

            if (( $param !== null ) && ( is_array($param) )) {

                foreach ($param as $key => $value) {
                    $fields[$key] = $value;
                }
            }

            return (array) $fields;
        } else {
            throw new JsonHeitzException(__METHOD__ . ' --> The type is empty !', 9);
        }
    }

    /**
     * @name lenghtOfString()
     * @access protected
     * @param string $string
     * @param int $maxSize
     * @return string
     * @throws JsonHeitzException
     */
    protected function lenghtOfString(string $string, int $maxSize)
    {
        if (strlen($string) > $maxSize) {
            throw new JsonHeitzException(__METHOD__ . ' --> The string is greater than "' . $maxSize . '" characters !', 30);
        } else {
            return (string) $string;
        }
    }

}
