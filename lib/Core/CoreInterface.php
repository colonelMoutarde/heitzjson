<?php

/*
 * Copyright (C) 2016 luc SANCHEZ.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *
 *  @author luc SANCHEZ <sanchezlucarobasegmaildotcom>
 * @copyright (c) 2016
 * @license GNU/LGPL http://www.gnu.org/licenses/lgpl.html 
 */

namespace JsonHeitzV2\Core;

interface CoreInterface
{

    /**
     * @name getCredential()
     */
    public function getCredential();
    /**
     * 
     * @param int $idSession
     * @param int $userId
     * @param type $type
     */
    public function setConnectHeitzAPI($idSession, $userId, $type);

    /**
     * 
     * @param string $newFields
     * @param string $pathToCookie
     */
    public function callWSHeitz(string $newFields, string $pathToCookie);
}
