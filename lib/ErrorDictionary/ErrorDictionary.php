<?php

/*
 * Copyright (C) 2016 luc SANCHEZ.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

namespace JsonHeitz\ErrorDictionary;

/**
 * @abstract List of Exception from Heitz System API
 *
 * @author luc SANCHEZ
 */
class ErrorDictionary
{

    /**
     *
     * @var string 
     */
    private $message = null;

    /**
     *
     * @var int 
     */
    private $idError = null;

    /**
     * @name getMessage()
     * @access public
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @name getIdError()
     * @access public
     * @return int
     */
    public function getIdError()
    {
        return $this->idError;
    }

    /**
     * @name setMessage()
     * @access public
     * @param string $message
     * @return \JsonHeitz\ErrorDictionary\ErrorDictionary
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @name setIdError()
     * @access public
     * @param int $idError
     * @return \JsonHeitz\ErrorDictionary\ErrorDictionary
     */
    public function setIdError($idError)
    {
        $this->idError = $idError;
        return $this;
    }

    /**
     * @name __construct()
     * @access public
     * @param int $idError
     * @return $this
     */
    public function __construct($idError)
    {
        $this->setIdError($idError);
        return $this->errorDictionary();
    }

    /**
     * @name ErrorDictionary::errorDictionary()
     * @access private
     * @return string value of error
     */
    private function errorDictionary()
    {

        switch ($this->getIdError()) {

            case 1 :
                $this->setMessage('Aucune session ouverte. Vous devez en premier ouvrir une session en effectuant une requête avec JsonHeitzV2::setConnectHeitzAPI().');
                break;

            case 2 :
                $this->setMessage('Erreur d\'authentification. Le paramètre "idSite" n\'est pas configuré dans le logiciel ou est vide.');
                break;

            case 3 :
                $this->setMessage('Le format n\'est pas celui attendu. L\'entête de la requête ou le contenu n\'est pas au format attendu.');
                break;

            case 4 :
                $this->setMessage('URL de requête manquante. Aucune URL de requête configurée ou présente lors de l\'appel (Paramètre requete).');
                break;

            case 5 :
                $this->setMessage('Erreur SQL. Une erreur lors de l\'exécution de la commande SQL. Vérifiez les paramètres de la requête.');
                break;

            case 6 :
                $this->setMessage('Type de page inconnue. Le paramètre type de l\'entête n\'est pas un des types prédéfinis.');
                break;

            case 7 :
                $this->setMessage('Compte employé nécessaire. La session doit être ouverte avec la fiche client d\'un employé.');
                break;

            case 8 :
                $this->setMessage('Droits insuffisants. L\'employé n\'a pas les droits pour afficher ces informations.');
                break;

            case 9 :
                $this->setMessage('L\'ID fourni n\'est pas correcte. La récupération de l\'ID nécessaire à l\'opération a échoué.');
                break;

            case 10 :
                $this->setMessage('Aucune donnée disponible. La requête n\'a renvoyé aucune donnée avec les paramètres fournis.');
                break;

            case 11 :
                $this->setMessage('Erreur non gérée. L\'exécution a généré une erreur non prévue.');
                break;

            case 12 :
                $this->setMessage('Erreur lors de l\'exécution de l\'action. L\'action demandée a généré une erreur lors de son exécution.');
                break;

            case 13 :
                $this->setMessage('Solde de point insuffisant. Le solde de point du client est insuffisant pour cette transaction.');
                break;

            case 14 :
                $this->setMessage('Fiche client en doublon. La mise à jour génère un doublon par rapport une autre fiche. Les critères de doublon sont définis par l\'établissement.');
                break;

            case 15 :
                $this->setMessage('Aucune modification demandée. Aucun champ ne peut être mis à jour. Cela peut être provoqué par la mise à jour des données avec des valeurs non autorisées.');
                break;

            case 101 :
                $this->setMessage('Les données pour l\'identification ne sont pas correctes. Vérifiez les données saisies. Les deux paramètres ne doivent pas être nuls.');
                break;

            case 102 :
                $this->setMessage('L\'identification ne renvoi pas de client. Aucun client ne correspond aux paramètres clients.');
                break;

            case 103 :
                $this->setMessage('Ancien mot de passe incorrect. Lors du changement du mot de passe l\'ancien ne correspond pas à celui de la base.');
                break;

            case 104 :
                $this->setMessage('Le nouveau mot de passe et sa confirmation sont différents. Lors du changement du mot de passe le nouveau et sa confirmation sont diffèrents');
                break;

            case 105 :
                $this->setMessage('L\'eMail ne correspond pas à celui de la fiche client. Lors du changement du mot de passe l\'email qui est envoyé ne correspond pas à celui se saisi sur la fiche client.');
                break;

            case 106 :
                $this->setMessage('L\'IBAN saisi n\'est pas correct');
                break;

            case 107 :
                $this->setMessage('Le BIC saisi n\'est pas correct');
                break;

            case 108 :
                $this->setMessage('Le nom de la banque n\'est pas valide');
                break;

            case 116 :
                $this->setMessage('Session non trouvée.');
                break;
            case 201 :
                $this->setMessage('Plus de place. Cette réservation ne possède plus de place.');
                break;

            case 202 :
                $this->setMessage('Limite de la tâche atteinte. Les limites de réservations définies pour la tâche sont atteintes');
                break;

            case 203 :
                $this->setMessage('Limite du groupe atteinte. Les limites de réservations définies pour le groupe de tâches sont atteintes.');
                break;

            case 204 :
                $this->setMessage('Réservation hors de la période. La période de réservation définie dans l\'établissment n\'est pas respectée.');
                break;

            case 205 :
                $this->setMessage('Aucun accès actif. Le client doit posséder un accès actif pour réserver cette activité');
                break;

            case 206 :
                $this->setMessage('Aucun accès nécessaire à la réservation. La réservation de cette activité nécessite d\'avoir un accès spécifique');
                break;

            case 207 :
                $this->setMessage('Délai minimum avant réservation non atteint. Le délai minimum avant que le client puisse réserver l\'activité n\'est pas atteint.');
                break;

            case 208 :
                $this->setMessage('Client déjà occupé. Le client possède déjà une réservation sur cette période.');
                break;

            case 209 :
                $this->setMessage('Nombre de place insuffisant. Le nombre de places disponibles est inférieur au nombre de places demandées.');
                break;

            case 210 :
                $this->setMessage('L’établissement est fermé. La réservation n’est autorisée que si l’établissement est ouvert');
                break;

            case 300 :
                $this->setMessage('Champ manquant. Un des champs obligatoires pour la création de la fiche client est manquant.');
                break;

            case 301 :
                $this->setMessage('Doublon. Un client existe déjà avec ce nom et ce prénom.');
                break;

            case 302 :
                $this->setMessage('Doublon email. Un client possède déjà une fiche avec cet email.');
                break;

            default : 0;
                $this->setMessage('La requête demandée s\'est déroulée correctement.');
        }

        return $this->getMessage();
    }

}
