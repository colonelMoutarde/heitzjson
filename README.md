Heitz system API JSON
========

A unofficial PHP class to connect to JSON with CURL to the "Heitz System" WebService.

Target version is 5.0+ of the API:
 **download the last version of [heitzWeb]( ftp://heitz.no-ip.com/version_12/Documentations/ ) on  heitz ftp **

Dont forget "try catch" ;-)

Tested with PHP 5.6.8 and PHP 7.2

licence GNU/LGPL

## Information
For addPassword() method, it's possible to use without connected first, it's interesting if the user has been forget is password.
This method send email to user from Heitz System.

## Install with composer
```json
require "json-heitz/json-heitz": "dev-master"
```
## Usage

```php

    <?php
    require_once 'vendor/autoload.php';

    use JsonHeitzV2\Queries\Queries;
    use JsonHeitz\Credential\Credential;
    use JsonHeitz\JsonHeitzException\JsonHeitzException;

try {
        $connect = ( new Credential())
                ->setPort(83)
                ->setUserLogin('test1@heitzsystem.com')
                ->setUserPass('1')
                ->setHost('http://heitz.no-ip.com')
                ->setHostPassword('AuthentificationBaseTest') //idSite
                ->urlWebServeurHeitz()
        ;
    } catch (JsonHeitzException $exc) {
        echo $exc->getMessage();
    }

    try {

        $call     = (new Queries($connect))
                ->setConnectHeitzAPI()
                ->setDebug(true) //optional
        ;
        $resultat = $call->callWSHeitz();

        $call->setIdSession($resultat->idSession)->setIdClient($resultat->idClient);

        var_dump($call->getOneClient());
        var_dump($call->getTaskList());
        var_dump($call->getTest());
        var_dump($call->getFormTraining());
        var_dump($call->getActiveReservation());
        var_dump($call->getConfigServeur());
        var_dump($call->getBilan());
        var_dump($call->getTest());
        var_dump($call->getClientMessage(10,15));
        var_dump($call->getListOfActiveAccess());
        var_dump($call->getListOfPassages());
        var_dump($call->getArrowPoints());
        var_dump($call->getTaskGroup());
        var_dump($call->getPlaceForTheTask());
        var_dump($call->getListOfFinancialDeadlines());
        var_dump($call->getListOfLevies());
        var_dump($call->getListOfBills());
        var_dump($call->getListOfFinancial());
        var_dump($call->getListOfFinancialDeadlines());
        var_dump($call->getListOfLevies());
        var_dump($call->getListOfPoints());
        var_dump($call->getListOfCures());
        var_dump($call->getListOfActiveAccess());
        var_dump($call->getAllArticles());
        var_dump($call->getAllCity());
        var_dump($call->getAllCivility());
        var_dump($call->getAllJobs());
        var_dump($call->getAllWhereTheCustomerHasPracticed());
        var_dump($call->getAllMotivation());
        var_dump($call->getAllVAT());
        var_dump($call->getAllPayment());
        var_dump($call->getAllWayToKnowTheInstitution());
        var_dump($call->getAllTypeOfProspects());
        var_dump($call->getAllDiscountLevel());
        var_dump($call->getAllCustomerGroup());
        var_dump($call->getAllEmployee());
        var_dump($call->getAllFamilySituation());
        var_dump($call->getArrowPoints());
        var_dump($call->getDetailOfCures());
        var_dump($call->getCriterion1());
        var_dump($call->getCriterion2());
        var_dump($call->getCriterion3());
        var_dump($call->getCriterion4());
        var_dump($call->getCriterion5());
        var_dump($call->accessListForAClient(100667943));
        var_dump($call->listOfClientsCreatedOver(new \DateTime('2015-01-10'), new \DateTime('2015-01-11')));
        var_dump($call->getClientIdByEmail('test1@heitzsystem.com'));
        var_dump($call->clientWithAnActiveSpecificAccess(101887774, 1000167));
        var_dump($call->listOfProspectsCreatedOver(new \DateTime('2015-01-10'), new \DateTime('2015-01-11')));
        var_dump($call->listOfProspectsConvertedOver(new \DateTime('2015-01-10'), new \DateTime('2015-01-11')));
    } catch (JsonHeitzException $exc) {
        echo $exc->getMessage() . $exc->getCode();
    }
    ?>
```

More info about the [Json Heitz SYSTEM API](https://www.heitzsystem.com/).
